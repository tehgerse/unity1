﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Boundary{
    public float xMin, xMax, zMin, zMax;
}

public class PlayerControls : MonoBehaviour
{
    public Boundary boundary;
    public float speed;
    public float tilt;

    public GameObject shot;
    public Transform shotSpawn;
    public float fireRate;

    private float nextFire ;


    void Update()
    {
        if(Input.GetButton("Jump") && Time.time > nextFire)
        {
            nextFire = Time.time + fireRate;
            
            Instantiate(shot, shotSpawn.position, shotSpawn.rotation);
        }
        
    }

    void FixedUpdate()
    {
        
        

        if (SystemInfo.deviceType == DeviceType.Desktop)
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical"); 

            Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

            GetComponent<Rigidbody>().velocity = movement * speed;
        }
        else
        {
            float moveH = Input.gyro.userAcceleration.x;
            float moveV = Input.gyro.userAcceleration.y;

            moveH = Input.acceleration.x;
            moveV = Input.acceleration.y;

            Vector3 movement = new Vector3(moveH, 0.0f, moveV);

            GetComponent<Rigidbody>().velocity = (movement * speed)*2;
        }

        GetComponent<Rigidbody>().position = new Vector3(
            Mathf.Clamp (GetComponent<Rigidbody>().position.x, boundary.xMin, boundary.xMax),
            0.0f,
            Mathf.Clamp(GetComponent<Rigidbody>().position.z, boundary.zMin, boundary.zMax)
        );

        GetComponent<Rigidbody>().rotation = Quaternion.Euler(
            0,0, GetComponent<Rigidbody>().velocity.x * -tilt
        );
    }

}
