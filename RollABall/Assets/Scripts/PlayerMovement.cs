﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {

    private Rigidbody rb;
    private int count;
    private Vector3 spawn;

    public float speed;
    public Text countText;
    public Text winText;
    public Vector3 userAcceleration;
    public GameObject deathParticles;

    void Start()
    {
        spawn = transform.position;
        rb = GetComponent<Rigidbody>();
        count = 0;
        setCountText();
        winText.text = "";
    }

    void Update()
    {

    }

    void FixedUpdate()
    {
        if (SystemInfo.deviceType == DeviceType.Desktop)
        {
            float moveHorizontal = Input.GetAxis("Horizontal");
            float moveVertical = Input.GetAxis("Vertical");

            Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

            rb.AddForce(movement * speed);
        }
        else
        {
            float moveH = Input.gyro.userAcceleration.x;
            float moveV = Input.gyro.userAcceleration.y;

            moveH = Input.acceleration.x;
            moveV = Input.acceleration.y;

            Vector3 movement = new Vector3(moveH, 0.0f, moveV);

            rb.AddForce((movement * speed * Time.deltaTime)*20);
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pickups"))
        {
            other.gameObject.SetActive(false);
            count++;
            setCountText();
        }
    }

    void setCountText()
    {
        countText.text = "Count: " + count.ToString();
        if(count >= 10)
        {
            winText.text = "You Win";

        }
    }

    void OnCollisionEnter(Collision other)
    {
        if(other.transform.tag == "Enemy")
        {
            Instantiate(deathParticles, transform.position, Quaternion.identity);
            transform.position = spawn;
        }
        

    }
}

    